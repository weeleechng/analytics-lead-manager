CREATE TABLE dbo.LeadManager
(
"rowId" bigint IDENTITY PRIMARY KEY,
"id" varchar(MAX),
"lastname" varchar(255),
"firstname" varchar(255),
"email" varchar(255),
"contactno" varchar(32),
"ipaddress" varchar(24),
"urlsource" varchar(2048),
"created_at" datetime,
"useragent" varchar(MAX),
"groupname" varchar(255)
);