<!DOCTYPE HTML>
<html>
<head>
<title>Lead Manager to Google Analytics</title>
<style>
body
{
  font-family: monospace;
  font-size: 14px;
}
</style>
</head>
<body>
<?php

$out_db = null;
$out_dsn = "CrunchOnAzure";
$out_username = 'Crunch@lyzz9ghpjh';
$out_password =  'Estorm123';
$out_table = 'dbo.LeadManager';

$in_db = null;
$in_host = '184.106.197.203';
$in_dbname = 'lm2014';
$in_username = 'postgres';
$in_pw = 'postgres';
$lead_table = 'lead';
$group_table = 'group';

$in_columns = [
  '"' . $lead_table . '".id',
  '"' . $lead_table . '".lastname',
  '"' . $lead_table . '".firstname',
  '"' . $lead_table . '".email',
  '"' . $lead_table . '".contactno',
  '"' . $lead_table . '".ipaddress',
  '"' . $lead_table . '".urlsource',
  '"' . $lead_table . '".created_at',
  '"' . $lead_table . '".useragent',
  '"' . $group_table . '".name',
];

$out_columns = [
  'id',
  'lastname',
  'firstname',
  'email',
  'contactno',
  'ipaddress',
  'urlsource',
  'created_at',
  'useragent',
  'groupname',
];

$columntypes = [];

$textcolumntypes = array (
  'text',
  'char',
  'varchar',
  'nvarchar',
  'datetime',
);

function connect_db_out ()
{
  global $out_db, $out_dsn, $out_username, $out_password;
  try
  {
    $out_db = odbc_connect ($out_dsn, $out_username, $out_password);
    return true;
  }
  catch (Exception $error)
  { 
    print ("There was an ERROR!<br />" . $error->getMessage());
    return false;
  }
}

function mssql_column_type ()
{
  global $out_db, $out_table, $out_columns, $columntypes;
  $query = 'SELECT TOP 1 "' . implode ('","', $out_columns) . '" FROM ' . $out_table;

  // $statement = $mssql_db->exec ($query);
  // $result = $statement->fetch (PDO::FETCH_ASSOC);
  $result = odbc_exec ($out_db, $query);
  for ($i = 1; $i <= odbc_num_fields ($result); $i++)
  {
    $columntypes[$i - 1] = odbc_field_type ($result, $i);
  }

}

function value_to_insert_array ($row_values)
{
  global $columns, $columntypes, $textcolumntypes;

  $insert_values = [];
  $index = 0;

  foreach ($row_values as $key => $value)
  {
    /* numerify strings */
    if (!in_array ($columntypes[$index], $textcolumntypes))
    {      
      $value = intval ($value);
    }
    else
    {
      $value = "'" . $value . "'";
    }

    $insert_values[] = $value;
    $index++;
  }

  return $insert_values;
}

function insert_db_out ($row_values)
{
  global $out_db, $out_table, $out_columns;

  $insert_sql = 'INSERT INTO ' . $out_table . ' ("' . implode ('","', $out_columns) . '") VALUES (' . implode (',', $row_values) . ')';

  if (isset ($_GET['storm_mode']) && $_GET['storm_mode'] == 'test')
  {
    var_dump ($insert_sql);
    return true;
  }

  try
  {
    $count = odbc_exec ($out_db, $insert_sql);

    if ($count > 0)
    {
      return true;
    }
    else
    {
      return false;
    }

  }
  catch (Exception $error)
  {
    print_r ($error->getMessage());
    return false;
  }
}

function close_db_out ()
{
  global $out_db;
  $out_db = null;
}

function connect_db_in ()
{
  global $in_db, $in_host, $in_dbname, $in_username, $in_pw;

  try
  {
    $in_db = new PDO ("pgsql:host=" . $in_host . ";dbname=" . $in_dbname . ";", $in_username, $in_pw);
    return true;
  }
  catch (Exception $error)
  {
    print ("There was an ERROR!<br />" . $error->getMessage());
    return false;
  }
}

function read_data_in ()
{
  global $in_db, $lead_table, $group_table, $in_columns;

  $querydate = (isset ($_GET['querydate'])) ? $_GET['querydate'] : date('Y-m-d', strtotime(' -1 day'));

  $condition = [
    '"lead".status' => "'pass'",
    '"lead".istestlead::boolean' => 'false',
    '"lead".created_at::date' => "'" . $querydate . "'",
    '"group".id' => '"lead".group',
  ];

  $conditionstr = " ";
  foreach ($condition as $key => $value)
  {
    if ($conditionstr != " ")
    {
      $conditionstr .= " AND ";
    }
    $conditionstr .= $key . ' = ' . $value . ' ';
  }

  // $columnstr = '"' . implode ('","', $in_columns) . '"';
  $columnstr = implode (',', $in_columns);

  $query = 'SELECT ' . $columnstr . ' FROM "' . $lead_table . '", "' . $group_table . '" WHERE';
  $query .= $conditionstr;
  $query .= ' ORDER BY "created_at" ASC';

  $promptstr = "<strong>LeadManager-Azure:</strong>";

  try
  {
    $stmt = $in_db->prepare ($query);
    $stmt->execute();

    $insertcount = 0;

    while ($row = $stmt->fetch (PDO::FETCH_ASSOC)) 
    {
      $row_values = [];
      foreach ($row as $key => $value)
      {
        $row_values[] = $value;
      }

      if (insert_db_out (value_to_insert_array ($row_values)) == false)
      {
        print ("<div>" . $promptstr . " ERROR inserting into external dB</div>");
        break;
      }
      else
      {
        print "<div>" . $promptstr . " Lead " . $row_values[0] . " inserted</div>";
        $insertcount++;
      }
    }

    if ($insertcount > 0)
    {
      print "<div>" . $promptstr . " Lead " . $insertcount . " records transferred</div>";
    }
    else
    {
      print "<div>" . $promptstr . " No records found for " . $querydate . "</div>"; 
    }

  }
  catch (Exception $error)
  {
    print ("<div>" . $promptstr . " There was an ERROR!<br />" . $error->getMessage() . "</div>");
    return false;
  }
 
}

function close_db_in ()
{
  global $in_db;
  $in_db = null;
}

connect_db_in ();
connect_db_out ();
mssql_column_type ();
read_data_in ();
close_db_in ();
close_db_out ();

?>
</body>
</html>
